<?php
namespace EvanSeabrook\SevenShifts\Tests;

use EvanSeabrook\SevenShifts\Adder\CustomDelimiterAdder;
use PHPUnit\Framework\TestCase;

class CustomAdderTest extends TestCase
{
    function testDelimiterFunctionality() {
        $testObj = new CustomDelimiterAdder();

        $val = $testObj->Add("//;\n1;2");
        $this->assertEquals(3, $val);

        $val = $testObj->Add("//aa\n3aa4aa5");
        $this->assertEquals(12, $val);

        $val = $testObj->Add("// \n3 4 5");
        $this->assertEquals(12, $val);
    }

    function testDelimiterOmitted() {
        $testObj = new CustomDelimiterAdder();
        $val = $testObj->Add("7,2,1,3");
        $this->assertEquals(13, $val);
    }

    function testNegativeThrowsException() {
        $testObj = new CustomDelimiterAdder();
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessageMatches("/Negatives not allowed: -1,-3/");
        $val = $testObj->Add("7,2,-1,-3");
    }

    function testNewlineDelimiter() {
        $testObj = new CustomDelimiterAdder();
        $val = $testObj->Add("//\n\n1\n2\n3");
        $this->assertEquals(6, $val);
    }

    function testNumbersLargerThanOneThousand() {
        $testObj = new CustomDelimiterAdder();
        $val = $testObj->Add("999,1000,1001,1002");
        $this->assertEquals("1999", $val);

        $val = $testObj->Add("1001");
        $this->assertEquals(0, $val);
    }

    function testMultipleDelimiters() {
        $testObj = new CustomDelimiterAdder();
        $val = $testObj->Add("//aa,b,ccc\n3aa4b5ccc9");
        $this->assertEquals(21, $val);
    }
}
