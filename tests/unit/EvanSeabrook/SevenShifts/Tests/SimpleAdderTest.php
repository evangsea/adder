<?php
namespace EvanSeabrook\SevenShifts\Tests;

use EvanSeabrook\SevenShifts\Adder\SimpleAdder;
use PHPUnit\Framework\TestCase;

class SimpleAdderTest extends TestCase
{
    function testEmptyString() {
        $objUnderTest = new SimpleAdder();
        $val = $objUnderTest->Add("");
        $this->assertEquals(0, $val, "Assert empty string yields 0");
    }

    function testExplicitSimpleAdder() {
        $objUnderTest = new SimpleAdder();

        $val = $objUnderTest->Add('3,5,8');
        $this->assertEquals(16, $val);

        $val = $objUnderTest->Add('-2,,70');
        $this->assertEquals(68, $val);

        $val = $objUnderTest->Add('1,2,3,4,5');
        $this->assertEquals(15, $val);

        $val = $objUnderTest->Add('1,2,' . PHP_EOL . ',4,5');
        $this->assertEquals(12, $val);
    }

    function testImplicitSimpleAdder() {
        $objUnderTest = new SimpleAdder();

        $numbersToTestString = "";
        $numbersToTest = [];

        for ($i = 0; $i < 100; $i++) {
            $num = $i*($i-1);
            $numbersToTest[] = $num;
            $numbersToTestString = ltrim($numbersToTestString . ",$num");

            $val = $objUnderTest->Add($numbersToTestString);

            $runningTotal = 0;
            foreach ($numbersToTest as $number) {
                $runningTotal += $number;
            }

            $this->assertEquals($runningTotal, $val);
        }
    }

}
