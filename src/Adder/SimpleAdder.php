<?php

namespace EvanSeabrook\SevenShifts\Adder;


use EvanSeabrook\SevenShifts\AdderInterface;

/**
 * Class SimpleAdder
 * @package EvanSeabrook\SevenShifts\Adder
 *
 * Simple adder to satisfy first exercise.
 */
class SimpleAdder implements AdderInterface
{
    /**
     * @param string $numbers
     * @return int
     */
    public function Add($numbers)
    {
        $numberGrams = explode(',', $numbers);

        $sum = 0;

        foreach ($numberGrams as $number) {
            $sum += (is_numeric($number) ? intval($number) : 0);
        }

        return $sum;
    }
}