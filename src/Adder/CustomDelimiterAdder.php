<?php
namespace EvanSeabrook\SevenShifts\Adder;

use EvanSeabrook\SevenShifts\AdderInterface;

class CustomDelimiterAdder implements AdderInterface
{
    /**
     * @var int Maximum input number to compute against
     */
    const MAX_INT = 1000;

    /**
     * @var string The delimiter to delimit... delimiters
     */
    const DELIM_DELIM = ',';

    /**
     * @var string The pattern to look out for when extracting delimiters
     */
    const DELIM_PATTERN = "/^\/\/.*\n/";

    /**
     * @inheritdoc
     */
    public function Add($numbers)
    {
        $matches = [];
        $matchFound = preg_match("/^\/\/(.*)\n/", $numbers, $matches);

        $sum = 0;
        $negativeNumbers = [];

        if ($matchFound) {
            $delimiter = $matches[1];
            $delimiters = explode(self::DELIM_DELIM, $matches[1]);

            if (!empty($delimiters)) {
                $this->mAdd($this->mSplit($delimiters,
                    preg_replace(self::DELIM_PATTERN, "", $numbers)), $negativeNumbers, $sum);
            } else {
                $this->mAdd($this->mSplit([$delimiter],
                    preg_replace(self::DELIM_PATTERN, "", $numbers)), $negativeNumbers, $sum);
            }
        } else {
            $this->mAdd($this->mSplit([','], $numbers), $negativeNumbers, $sum);
        }

        if (!empty($negativeNumbers)) {
            throw new \InvalidArgumentException(
                sprintf("Negatives not allowed: %s", implode(',', $negativeNumbers))
            );
        }

        return $sum;
    }

    /**
     * @param array $delimiters
     * @param string $numbers
     * @return array
     */
    private function mSplit($delimiters, $numbers) {
        foreach ($delimiters as $delimiter) {
            if (empty($delimiter)) $delimiter = PHP_EOL;
            $numbers = str_replace($delimiter, ',', $numbers);
        }

        return explode(',', $numbers);
    }

    /**
     * Compute sum of integers found in $numbers
     * @param array $numberGrams
     * @param array $negativeNumbers
     * @param int $sum
     */
    private function mAdd($numberGrams, &$negativeNumbers, &$sum)
    {
        foreach ($numberGrams as $number) {
            if (is_numeric($number) && $number < 0) {
                $negativeNumbers[] = $number;
            }
            $sum += ((is_numeric($number) && $number <= self::MAX_INT) ? intval($number) : 0);
        }
    }
}