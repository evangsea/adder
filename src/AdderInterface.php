<?php
namespace EvanSeabrook\SevenShifts;

interface AdderInterface
{
    /**
     * Evaluate a list of numbers and return the sum
     * @param string $numbers
     * @return int
     */
    function Add($numbers);
}