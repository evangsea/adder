# Adder

## Installation
Running this project requires a machine with PHP 5.6 and a compatible version of [composer](http://getcomposer.org) installed.  
To install, clone this project and run `composer install`.

## Test Invocation
To run the tests, simply execute `./vendor/bin/phpunit` from the project's root directory.